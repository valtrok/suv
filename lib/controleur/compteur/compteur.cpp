#include <EEPROM.h>
#include "compteur.h"


ControleurCompteur::ControleurCompteur() {
    unsigned long time = millis();
    this->gps = GPS();
    this->lcd = LCD();
    this->btn = boutton();
    this->state = COURANT;
    this->save_start_time = time;
    this->compute_start_time = time;
    this->refresh_start_time = time;
    this->button_state = false;

    double t_d, a_s;
    unsigned int s_s;
    EEPROM.get(SPEED_SAMPLES_ADDRESS, s_s);
    EEPROM.get(TOTAL_DISTANCE_ADDRESS, t_d);
    EEPROM.get(AVERAGE_SPEED_ADDRESS, a_s);
    this->speed_samples = s_s;
    this->total_distance = t_d;
    this->average_speed = a_s;
}

void ControleurCompteur::recomputeAverageSpeed() {
    if(this->speed_samples < 1000) {
        this->speed_samples++;
    }
    this->average_speed *= this->speed_samples - 1;
    this->average_speed += this->current_speed;
    this->average_speed /= this->speed_samples;
}

void ControleurCompteur::run() {
    unsigned long time = millis();
    if (this->btn.getArriere()) {
        if (!this->button_state) {
            this->button_state = true;
            switch(this->state) {
                case COURANT:
                case WAIT_GPS:
                    this->state = TOTAL;
                    break;
                case TOTAL:
                    this->state = COURANT;
                    break;
            }
            this->refresh_start_time = time + REFRESH_DELAY;
        }
    } else {
        this->button_state = false;
        if (time - this->compute_start_time >= COMPUTE_DELAY) {
            this->compute_start_time = time;
            if (gps.isTriangulated()) {
                if (this->state == WAIT_GPS) {
                    this->state = COURANT;
                }
                this->current_speed = this->gps.getVitesse();
                double distance = this->current_speed/3600.0*COMPUTE_DELAY/1000.0;
                this->current_distance += distance;
                this->total_distance += distance;
                this->recomputeAverageSpeed();
            } else if (this->state == COURANT) {
                this->state = WAIT_GPS;
            }
        }
        if (time - this->refresh_start_time >= REFRESH_DELAY) {
            this->refresh_start_time = time;
            switch(this->state) {
                case WAIT_GPS:
                    this->lcd.printWaitGPS();
                    break;
                case COURANT:
                    this->lcd.printDistAndSpeed(this->current_speed, this->current_distance, false);
                    break;
                case TOTAL:
                    this->lcd.printDistAndSpeed(this->average_speed, this->total_distance, true);
                    break;
            }
        }
        if (time - this->save_start_time >= SAVE_DELAY) {
            this->save_start_time = time;
            EEPROM.put(SPEED_SAMPLES_ADDRESS, this->speed_samples);
            EEPROM.put(TOTAL_DISTANCE_ADDRESS, this->total_distance);
            EEPROM.put(AVERAGE_SPEED_ADDRESS, this->average_speed);
        }
    }
}