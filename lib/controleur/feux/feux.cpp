#include "feux.h"
#include <Arduino.h>

ControleurFeux::ControleurFeux() {
    // ACCELEROMETRE
    this->acc = Accelerometre();
    this->virage_gauche = false;
    this->virage_droite = false;
    // BOUTONS
    this->btn = boutton();
    this->state_boutons.gauche = false;
    this->state_boutons.droite = false;
    // FEUX
    this->leds = LEDS();
    this->state_feux.droite = false;
    this->state_feux.gauche = false;
    this->time_gauche = 0;
    this->time_droite = 0;
}



/*********************/
/*  GESTION BOUTONS  */
/*********************/

void ControleurFeux::run() {
    // CLIGNOTANTS
    this->clignoterGauche();
    this->clignoterDroite();
    // Bouton Gauche
    if (this->btn.getGauche()) {
        if (!this->state_boutons.gauche) {
            this->state_boutons.gauche = true;
            this->arreterDroite();
            // modifier machine à état
            this->state_feux.gauche = !this->state_feux.gauche;
            // éviter que la led reste allumée
            this->leds.eteindreGauche();
        }
    } else {
        this->state_boutons.gauche = false;
        // Bouton droit
        if(this->btn.getDroite()) {
            if(!this->state_boutons.droite) {
                this->state_boutons.droite = true;
                this->arreterGauche();
                // modifier machine à état
                this->state_feux.droite = !this->state_feux.droite;
                // éviter que la led reste allumée
                this->leds.eteindreDroite();
            }
        } else {
            this->state_boutons.droite = false;
        }
    }
    // ACCELEROMETRE
    // gauche
    if(this->state_feux.gauche) {
        unsigned long time = millis();
        if(time - this->time_acc > ACC_REFRESH_DELAY) {
            this->time_acc = time;
            if(this->virage_gauche) {
                if(acc.getX() < MOYENNE_ACC + ACC_THRESHOLD) {
                    this->virage_gauche = false;
                    this->state_feux.gauche = false;
                    this->arreterGauche();
                    this->leds.eteindreGauche();
                }
            } else if(acc.getX() > MOYENNE_ACC + ACC_THRESHOLD) {
                this->virage_gauche = true;
            }
        }
    }
    // droite
    if(this->state_feux.droite) {
        unsigned long time = millis();
        if(time - this->time_acc > ACC_REFRESH_DELAY) {
            this->time_acc = time;
            if(this->virage_droite) {
                if(acc.getX() > MOYENNE_ACC - ACC_THRESHOLD) {
                    this->virage_droite = false;
                    this->state_feux.droite = false;
                    this->arreterDroite();
                    this->leds.eteindreDroite();
                }
            } else if(acc.getX() < MOYENNE_ACC - ACC_THRESHOLD) {
                this->virage_droite = true;
            }
        }
    }
}

/******************/
/*  GESTION FEUX  */
/******************/

void ControleurFeux::clignoterGauche() {
    if(this->state_feux.gauche) {
        if(millis() - this->time_gauche >= 500) {
            this->time_gauche = millis();
            this->leds.toggleGauche();
        }
    }
}

void ControleurFeux::clignoterDroite() {
    if(this->state_feux.droite) {
        if(millis() - this->time_droite >= 500) {
            this->time_droite = millis();
            this->leds.toggleDroite();
        }
    }
}

void ControleurFeux::arreterDroite() {
    if(this->state_feux.droite) {
        this->state_feux.droite = false;
        this->leds.eteindreDroite();
    }
}

void ControleurFeux::arreterGauche() {
    if(this->state_feux.gauche) {
        this->state_feux.gauche = false;
        this->leds.eteindreGauche();
    }
}

void ControleurFeux::warnings() {
    // sortir si état inchangé
    if(this->state_feux.droite && this->state_feux.gauche) return;
    // modifier machine à état
    this->state_feux.gauche = true;
    this->state_feux.droite = true;
    // allumer les warnings
    this->clignoterGauche();
    this->clignoterDroite();
}