#include "api.h"
#include "ControleurInterface.h"
#define MOYENNE_ACC 364
#define ACC_THRESHOLD 20
#define ACC_REFRESH_DELAY 100

struct StateFeux {
    bool gauche;
    bool droite;
    bool arriere;
};

struct StateBoutons {
    bool gauche;
    bool droite;
};

class ControleurFeux : public ControleurInterface {
    private:
        // ACCELEROMETRE
        Accelerometre acc;
        unsigned long time_acc;
        bool virage_gauche, virage_droite;
        // BOUTONS
        boutton btn;
        struct StateBoutons state_boutons;
        // FEUX
        struct StateFeux state_feux;
        LEDS leds;
        unsigned long time_gauche, time_droite;
        // Clignotants
        void clignoterGauche();
        void clignoterDroite();
        // Arreter les clignotants
        void arreterDroite();
        void arreterGauche();
        // warnings
        void eteindreArriere();
        void warnings();
    public:
        ControleurFeux();
        void run();
};