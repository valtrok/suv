#include <Wire.h>
#include <Arduino.h>
#define GPM 0x68  	                              // GPM I2C Address
#define byte uint8_t

class GPS {
    public:
        float getVitesse();
        bool isTriangulated();
    private:
        int GetDouble();
        int GetSingle();
};