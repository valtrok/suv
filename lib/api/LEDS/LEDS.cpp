#include "LEDS.h"
#include <Arduino.h>

LEDS::LEDS(void) {
    pinMode(LED_gauche_PIN, OUTPUT);
    pinMode(LED_droite_PIN, OUTPUT);
    pinMode(LED_arriere_PIN, OUTPUT);
}

bool LEDS::getGauche(){
    return state.gauche;
}
bool LEDS::getDroite(){
    return state.droite;
}
bool LEDS::getArriere(){
    return state.arriere;
}

void LEDS::allumerGauche(){
    digitalWrite(LED_gauche_PIN, HIGH);
    state.gauche = true;
}
void LEDS::allumerDroite(){
    digitalWrite(LED_droite_PIN, HIGH);
    state.droite = true;
}
void LEDS::allumerArriere(){
    digitalWrite(LED_arriere_PIN, HIGH);
    state.arriere = true;
}

void LEDS::eteindreGauche(){
    digitalWrite(LED_gauche_PIN, LOW);
    state.gauche = false;
}
void LEDS::eteindreDroite(){
    digitalWrite(LED_droite_PIN, LOW);
    state.droite = false;
}
void LEDS::eteindreArriere(){
    digitalWrite(LED_arriere_PIN, LOW);
    state.arriere = false;
}

void LEDS::toggleGauche(){
    if (getGauche()){
        eteindreGauche();
    }
    else allumerGauche();
}
void LEDS::toggleDroite(){
     if (getDroite()){
        eteindreDroite();
    }
    else allumerDroite();
}