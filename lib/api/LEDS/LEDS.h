#define LED_gauche_PIN 11
#define LED_droite_PIN 12
#define LED_arriere_PIN 13

struct StateLEDS {
    bool gauche;
    bool droite;
    bool arriere;
};

class LEDS {
    private:
        struct StateLEDS state = {false, false, false};
    public:
        LEDS();
        bool getDroite();
        bool getGauche();
        bool getArriere();
        // allumer
        void allumerGauche();
        void allumerDroite();
        void allumerArriere();
        // Eteindre
        void eteindreGauche();
        void eteindreDroite();
        void eteindreArriere();
        // Toggle
        void toggleGauche();
        void toggleDroite();
};