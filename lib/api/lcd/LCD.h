#include <Arduino.h>
#include "U8glib.h"

#define CLK_PIN 8
#define DIN_PIN 4
#define CE_PIN 7
#define DC_PIN 5
#define RST_PIN 6

class LCD {
    public:
        LCD();
        void printDistAndSpeed(double speed, double dist, bool average);
        void printWaitGPS();
};