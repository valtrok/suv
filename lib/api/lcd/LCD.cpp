#include "LCD.h"

char speed_text[11];
char dist_text[18];
char speed_str[6];
char dist_str[8];
char dots_str[17];
U8GLIB_PCD8544 u8g(CLK_PIN, DIN_PIN, CE_PIN, DC_PIN, RST_PIN);

LCD::LCD(void) {
    dots_str[0] = '\0';
}

void LCD::printDistAndSpeed(double speed, double dist, bool average) {
    u8g.firstPage();
    dtostrf(speed, 1, 1, speed_str);
    sprintf(speed_text, "%skm/h", speed_str);
    if (average) {
        dtostrf(dist, 1, 1, dist_str);
        sprintf(dist_text, "Total : %skm", dist_str);    
    } else {
        dtostrf(dist, 1, 1, dist_str);
        sprintf(dist_text, "Dist : %skm", dist_str);
    }
    do {  
        u8g.setFont(u8g_font_tpss);
        u8g.drawStr(0, 13, dist_text);
        u8g.drawHLine(0, 15, 84);
        u8g.setFont(u8g_font_fur14);
        u8g.drawStr(0, 38, speed_text);
        if (average) {
            u8g.setFont(u8g_font_6x12);
            u8g.drawStr(65, 45, "moy");
        }
    } while(u8g.nextPage());
}

void LCD::printWaitGPS() {
    u8g.firstPage();
    if (strlen(dots_str) < (sizeof dots_str) - 1) {
        dots_str[strlen(dots_str)] = '.';
        if (strlen(dots_str) >= ((sizeof dots_str) - 1)/3) {
            dots_str[strlen(dots_str) - ((sizeof dots_str) - 1)/3] = ' ';
        }
    } else {
        memset(dots_str, 0, sizeof dots_str);
        dots_str[0] = '.';
    }
    do {
        u8g.setFont(u8g_font_fur14);
        u8g.drawStr(0, 9, dots_str);
        u8g.drawHLine(0, 15, 84);
        u8g.drawStr(0, 38, "GPS");
        u8g.setFont(u8g_font_6x12);
        u8g.drawStr(10, 45, "initializing");
    } while(u8g.nextPage());
}