#include "Boutton.h"
#include <Arduino.h>
boutton::boutton(void) {
    pinMode(BOUTTON_gauche_PIN, INPUT);
    pinMode(BOUTTON_droite_PIN, INPUT);
    pinMode(BOUTTON_arriere_PIN, INPUT);
}
bool boutton::getGauche(){
    return 1-digitalRead(BOUTTON_gauche_PIN);
}
bool boutton::getDroite(){
    return 1-digitalRead(BOUTTON_droite_PIN);
}
bool boutton::getArriere(){
    return 1-digitalRead(BOUTTON_arriere_PIN);
}
