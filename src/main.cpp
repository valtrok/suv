#include <Arduino.h>
#include "api.h"
#include "controleur.h"

ControleurCompteur compteur;
ControleurFeux feux;
ControleurStop stop;

void setup() {
    compteur = ControleurCompteur();
    feux = ControleurFeux();
    stop = ControleurStop();
}

void loop() {
    compteur.run();
    feux.run();
    stop.run();
}